# Use of technologies
Butter Knife  
Lombok  

## Used language
Java  
Kotlin  

## Butter Knife
auth/RegisterActivity.java  
auth/LoginActivity.java  
ChatActivity.java  
ProfileActivity.java  
SettingsActivity.java  
StartActivity.java  
  
## Lombok
Chats.java  
Friends.java  
Messages.java  
Requests.java  
Users.java

---
author: Melichar Jan   
---

