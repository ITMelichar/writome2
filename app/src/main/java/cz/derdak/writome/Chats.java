package cz.derdak.writome;

import lombok.Data;

public @Data
class Chats {

    public boolean seen;
    public long timestamp;
    String message;

}
