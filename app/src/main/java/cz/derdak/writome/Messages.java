package cz.derdak.writome;

import lombok.Data;

public @Data
class Messages {

    private String message, type, from;
    private long time;
    private boolean isSeen;

}
