package cz.derdak.writome;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.derdak.writome.auth.LoginActivity;
import cz.derdak.writome.auth.RegisterActivity;

public class StartActivity extends AppCompatActivity {

    @BindView(R.id.btn_registration) Button mRegBtn;
    @BindView(R.id.btn_login) Button mLogBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);

        mRegBtn.setOnClickListener(view -> {

            Intent reg_intent = new Intent(StartActivity.this, RegisterActivity.class);
            startActivity(reg_intent);
        });

        mLogBtn.setOnClickListener(view -> {

            Intent log_intent = new Intent(StartActivity.this, LoginActivity.class);
            startActivity(log_intent);
        });

    }
}
