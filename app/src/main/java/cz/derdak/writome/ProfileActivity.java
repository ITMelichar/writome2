package cz.derdak.writome;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.profile_image) ImageView mProfileImage;
    @BindView(R.id.displayName) TextView mDisplayName;
    @BindView(R.id.displayStatus) TextView mDisplayStatus;
    @BindView(R.id.profile_send_req_btn) Button mProfileSendReq;
    @BindView(R.id.profile_decline_req_btn) Button mProfileDeclineReq;

    private DatabaseReference mFriendsReqDatabase;
    private DatabaseReference mFriendDatabase;

    private DatabaseReference mRootRef;

    private FirebaseUser mCurrent_user;

    private ProgressDialog mProgressDialog;

    private String mCurrent_state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar mToolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String user_id = getIntent().getStringExtra("user_id");

        mRootRef = FirebaseDatabase.getInstance().getReference();


        assert user_id != null;
        DatabaseReference mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
        mFriendsReqDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mFriendDatabase = FirebaseDatabase.getInstance().getReference().child("Friends");

        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();

        ButterKnife.bind(this);

        mProfileDeclineReq.setVisibility(View.INVISIBLE);
        mProfileDeclineReq.setEnabled(false);

        mCurrent_state = "not_friends";

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Loading User Data");
        mProgressDialog.setMessage("Please wait while we load the user data.");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String display_name = Objects.requireNonNull(dataSnapshot.child("name").getValue()).toString();
                String display_status = Objects.requireNonNull(dataSnapshot.child("status").getValue()).toString();
                String image = Objects.requireNonNull(dataSnapshot.child("image").getValue()).toString();

                mDisplayName.setText(display_name);
                mDisplayStatus.setText(display_status);

                if (image.equals("default_male")) {
                    //  Picasso.with(ProfileActivity.this).load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(mProfileImage);
                    Picasso.get().load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(mProfileImage);
                } else if (image.equals("default_female")) {
                    //  Picasso.with(ProfileActivity.this).load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(mProfileImage);
                    Picasso.get().load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(mProfileImage);
                } else {
                    //  Picasso.with(ProfileActivity.this).load(image).placeholder(R.drawable.avatar_other).into(mProfileImage);
                    Picasso.get().load(image).placeholder(R.drawable.avatar_other).into(mProfileImage);
                }

                // FRIENDS LIST / REQUEST FEATURE
                mFriendsReqDatabase.child(mCurrent_user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.hasChild(user_id)) {

                            String req_type = Objects.requireNonNull(dataSnapshot.child(user_id).child("request_type").getValue()).toString();

                            if (req_type.equals("received")) {

                                mCurrent_state = "req_received";
                                mProfileSendReq.setText(R.string.accept_friend_req);

                                mProfileDeclineReq.setVisibility(View.VISIBLE);
                                mProfileDeclineReq.setEnabled(true);

                            } else if (req_type.equals("sent")) {

                                mCurrent_state = "req_sent";
                                mProfileSendReq.setText(R.string.cancel_friend_req);

                                mProfileDeclineReq.setVisibility(View.INVISIBLE);
                                mProfileDeclineReq.setEnabled(false);

                            }


                            mProgressDialog.dismiss();

                        } else {

                            mFriendDatabase.child(mCurrent_user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    if (dataSnapshot.hasChild(user_id)) {

                                        mCurrent_state = "Friends";
                                        mProfileSendReq.setText(R.string.unfriend_user);

                                        mProfileDeclineReq.setVisibility(View.INVISIBLE);
                                        mProfileDeclineReq.setEnabled(false);

                                    }

                                    mProgressDialog.dismiss();

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                    mProgressDialog.dismiss();

                                }
                            });

                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mProfileSendReq.setOnClickListener(v -> {

            mProfileSendReq.setEnabled(false);

            // NOT FRIENDS STATE

            if (mCurrent_state.equals("not_friends")) {

                DatabaseReference newNotificationRef = mRootRef.child("notifications").child(user_id).push();
                String newNotificationId = newNotificationRef.getKey();

                HashMap<String, String> notificationsData = new HashMap<>();
                notificationsData.put("from", mCurrent_user.getUid());
                notificationsData.put("type", "request");

                Map<String, Object> requestMap = new HashMap<>();
                requestMap.put("Friend_req/" + mCurrent_user.getUid() + "/" + user_id + "/request_type", "sent");
                requestMap.put("Friend_req/" + user_id + "/" + mCurrent_user.getUid() + "/request_type", "received");
                requestMap.put("notifications/" + user_id + "/" + newNotificationId, notificationsData);

                mRootRef.updateChildren(requestMap, (databaseError, databaseReference) -> {

                    if (databaseError != null) {

                        Toast.makeText(ProfileActivity.this, "Problém v odesílání žádosti", Toast.LENGTH_SHORT).show();


                    }

                    mProfileSendReq.setEnabled(true);
                    mCurrent_state = "req_sent";
                    mProfileSendReq.setText(R.string.cancel_friend_req);

                });
            }

            // CANCEL REQUEST STATE

            if (mCurrent_state.equals("req_sent")) {

                mFriendsReqDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue().addOnSuccessListener(aVoid -> mFriendsReqDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue().addOnSuccessListener(aVoid1 -> {

                    mProfileSendReq.setEnabled(true);
                    mCurrent_state = "not_friends";
                    mProfileSendReq.setText(R.string.send_friend_req);

                    mProfileDeclineReq.setVisibility(View.INVISIBLE);
                    mProfileDeclineReq.setEnabled(false);


                }));


            }

            // REQUEST RECEIVED

            if (mCurrent_state.equals("req_received")) {

                final String currentDate = DateFormat.getDateInstance().format(new Date());

                Map<String, Object> friendsMap = new HashMap<>();
                friendsMap.put("Friends/" + mCurrent_user.getUid() + "/" + user_id + "/date", currentDate);
                friendsMap.put("Friends/" + user_id + "/" + mCurrent_user.getUid() + "/date", currentDate);

                friendsMap.put("Friend_req/" + mCurrent_user.getUid() + "/" + user_id, null);
                friendsMap.put("Friend_req/" + user_id + "/" + mCurrent_user.getUid(), null);

                mRootRef.updateChildren(friendsMap, (databaseError, databaseReference) -> {

                    if (databaseError == null) {

                        mProfileSendReq.setEnabled(true);
                        mCurrent_state = "Friends";
                        mProfileSendReq.setText(R.string.unfriend_user);

                        mProfileDeclineReq.setVisibility(View.INVISIBLE);
                        mProfileDeclineReq.setEnabled(false);

                    } else {

                        String error = databaseError.getMessage();

                        Toast.makeText(ProfileActivity.this, error, Toast.LENGTH_SHORT).show();


                    }

                });
            }

            //UNFRIEND

            if (mCurrent_state.equals("Friends")) {

                Map<String, Object> unfriendsMap = new HashMap<>();
                unfriendsMap.put("Friends/" + mCurrent_user.getUid() + "/" + user_id, null);
                unfriendsMap.put("Friends/" + user_id + "/" + mCurrent_user.getUid(), null);

                mRootRef.updateChildren(unfriendsMap, (databaseError, databaseReference) -> {

                    if (databaseError == null) {

                        mCurrent_state = "not_friends";
                        mProfileSendReq.setText(R.string.send_friend_req);

                        mProfileDeclineReq.setVisibility(View.INVISIBLE);
                        mProfileDeclineReq.setEnabled(false);

                    } else {

                        String error = databaseError.getMessage();

                        Toast.makeText(ProfileActivity.this, error, Toast.LENGTH_SHORT).show();


                    }


                    mProfileSendReq.setEnabled(true);

                });

            }

        });

    }
}
