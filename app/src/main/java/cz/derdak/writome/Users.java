package cz.derdak.writome;

import lombok.Data;

public @Data
class Users {

    public String name;
    String status;
    private String image;
    private String thumb_image;

}
