package cz.derdak.writome;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class AdminActivity extends AppCompatActivity {

    private DatabaseReference mRootRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Toolbar mToolbar = findViewById(R.id.admin_toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRootRef = FirebaseDatabase.getInstance().getReference();

        // Components
        Button btnRemoveNot = findViewById(R.id.btn_resetNot);
        Button btnRemoveReq = findViewById(R.id.btn_resetReq);
        Button btnRemoveChat = findViewById(R.id.btn_resetChat);
        Button btnRemoveMess = findViewById(R.id.btn_resetMess);
        Button btnRemoveAll = findViewById(R.id.btn_resetAll);


        btnRemoveNot.setOnClickListener(v -> {

            mRootRef.child("notifications").removeValue();
            Toast.makeText(AdminActivity.this, "Notifications have been deleted!", Toast.LENGTH_SHORT).show();


        });

        btnRemoveReq.setOnClickListener(v -> {

            mRootRef.child("Friend_req").removeValue();
            Toast.makeText(AdminActivity.this, "Friend requests have been deleted!", Toast.LENGTH_SHORT).show();


        });

        btnRemoveChat.setOnClickListener(v -> {

            mRootRef.child("Chat").removeValue();
            Toast.makeText(AdminActivity.this, "Chats have been deleted!", Toast.LENGTH_SHORT).show();


        });

        btnRemoveMess.setOnClickListener(v -> {

            mRootRef.child("messages").removeValue();
            Toast.makeText(AdminActivity.this, "Messages have been deleted!", Toast.LENGTH_SHORT).show();


        });

        btnRemoveAll.setOnClickListener(v -> {

            mRootRef.child("Friend_Req").removeValue();
            mRootRef.child("messages").removeValue();
            mRootRef.child("Chat").removeValue();
            mRootRef.child("notifications").removeValue();
            mRootRef.child("Friends").removeValue();

            Toast.makeText(AdminActivity.this, "Database has been deleted!", Toast.LENGTH_SHORT).show();


        });


    }
}

