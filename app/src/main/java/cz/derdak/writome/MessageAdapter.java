package cz.derdak.writome;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    private List<Messages> messageList;


    MessageAdapter(List<Messages> messageList) {

        this.messageList = messageList;

    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_message_layout, parent, false);

        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {

        String current_user_id = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();

        Messages c = messageList.get(position);

        String from_user = c.getFrom();
        String message_type = c.getType();
        long time = c.getTime();

        String pattern = "HH:mm -dd.MM.yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Timestamp ts = new Timestamp(time);

        String datum = simpleDateFormat.format(ts);

        DatabaseReference mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(from_user);

        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {

                holder.nameText.setText(Objects.requireNonNull(dataSnapshot.child("name").getValue()).toString());

            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });


        if (message_type.equals("image")) {

            holder.messageImage.setVisibility(View.VISIBLE);

            holder.messageText.setVisibility(View.INVISIBLE);

            Picasso.get().load(c.getMessage()).placeholder(R.drawable.avatar_other).into(holder.messageImage);

        } else {

            holder.messageImage.setVisibility(View.INVISIBLE);

            if (from_user.equals(current_user_id)) {

                holder.messageText.setBackgroundResource(R.drawable.message_text_bg2);
                holder.messageText.setTextColor(Color.WHITE);

            } else {

                holder.messageText.setBackgroundResource(R.drawable.message_text_bg);
                holder.messageText.setTextColor(Color.WHITE);

            }

            if (message_type.equals("url")) {
                holder.messageText.setTextColor(Color.CYAN);
                (holder.messageText).setOnClickListener(v -> {
                    String url = c.getMessage();
                    Log.i("messageToUrl", url);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    v.getContext().startActivity(browserIntent);
                });
            }

        }
        holder.timeText.setText(datum);
        holder.messageText.setText(c.getMessage());

    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }


    class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView messageText, nameText, timeText;
        ImageView messageImage;

        MessageViewHolder(View v) {
            super(v);

            messageText = v.findViewById(R.id.message_text_view);
            nameText = v.findViewById(R.id.message_name_view);
            timeText = v.findViewById(R.id.message_time_view);
            messageImage = v.findViewById(R.id.message_image_view);


        }
    }
}
