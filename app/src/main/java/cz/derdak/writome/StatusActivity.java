package cz.derdak.writome;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class StatusActivity extends AppCompatActivity {

    private DatabaseReference mStatusDatabase;

    private TextInputLayout mStatus;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        FirebaseUser mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        assert mCurrentUser != null;
        String uid = mCurrentUser.getUid();

        mStatusDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


        Toolbar mToolbar = findViewById(R.id.status_appbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.acc_status);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String status_value = getIntent().getStringExtra("status_value");

        mStatus = findViewById(R.id.status_input);

        Objects.requireNonNull(mStatus.getEditText()).setText(status_value);

        Button mSavebtn = findViewById(R.id.status_save_btn);


        mSavebtn.setOnClickListener(view -> {


            mProgress = new ProgressDialog(StatusActivity.this);
            mProgress.setTitle(R.string.saving_changes);
            mProgress.setMessage(getResources().getString(R.string.saving_changes_mess));
            mProgress.show();


            String status = mStatus.getEditText().getText().toString();

            mStatusDatabase.child("status").setValue(status).addOnCompleteListener(task -> {

                if (task.isSuccessful()) {

                    mProgress.dismiss();
                    Intent goBack = new Intent(StatusActivity.this, SettingsActivity.class);
                    startActivity(goBack);

                } else {

                    Toast.makeText(getApplicationContext(), R.string.err, Toast.LENGTH_SHORT).show();
                }
            });


        });


    }
}
