package cz.derdak.writome;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class SettingsActivity extends AppCompatActivity {

    private static final int GALLERY_PICK = 1;
    DatabaseReference mRootRef;
    private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;
    private String mCurrent_user;
    private StorageReference mImageStorageRef;

    private ProgressDialog mProgressDialog;

    @BindView(R.id.circleImage) CircleImageView mDisplayImage;
    @BindView(R.id.displayID) TextView mName;
    @BindView(R.id.sett_text_about) TextView mStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mImageStorageRef = FirebaseStorage.getInstance().getReference();

        ButterKnife.bind(this);

        FirebaseUser mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        assert mCurrentUser != null;
        String uid = mCurrentUser.getUid();

        Toolbar mToolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.acc_sett);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
        mUserDatabase.keepSynced(true);


        mProgressDialog = new ProgressDialog(SettingsActivity.this);
        mProgressDialog.setTitle(R.string.loading_profile);
        mProgressDialog.setMessage(getResources().getString(R.string.loading_profile_mess));
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {

                String name = Objects.requireNonNull(dataSnapshot.child("name").getValue()).toString();
                final String image = Objects.requireNonNull(dataSnapshot.child("thumb_image").getValue()).toString();
                String status = Objects.requireNonNull(dataSnapshot.child("status").getValue()).toString();

                mName.setText(name);
                mStatus.setText(status);

                if (image.equals("default_male")) {
                    Picasso.get().load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(mDisplayImage);
                } else if (image.equals("default_female")) {
                    Picasso.get().load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(mDisplayImage);
                } else {
                    Picasso.get().load(image).placeholder(R.drawable.avatar_other).into(mDisplayImage);
                }

                mProgressDialog.dismiss();

            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });


        Button mAbout = findViewById(R.id.sett_about_btn);

        mAbout.setOnClickListener(view -> {

            String status_value = mStatus.getText().toString();

            Intent statusActivity = new Intent(SettingsActivity.this, StatusActivity.class);
            statusActivity.putExtra("status_value", status_value);

            startActivity(statusActivity);

        });

        Button btnDeleteUser = findViewById(R.id.btn_deleteUser);

        mAuth = FirebaseAuth.getInstance();

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mCurrent_user = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();

        btnDeleteUser.setOnClickListener(v -> {

            mCurrent_user = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

            mRootRef.child("Users").child(mCurrent_user).setValue(null);
            mAuth.getCurrentUser().delete();
            FirebaseAuth.getInstance().signOut();
            finish();

        });

        Button mImage = findViewById(R.id.sett_image_btn);

        mImage.setOnClickListener(view -> {

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_PICK);

        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {

            Uri imageUri = data.getData();

            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .setMinCropWindowSize(500, 500)
                    .start(this);


        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {


                mProgressDialog = new ProgressDialog(SettingsActivity.this);
                mProgressDialog.setTitle(R.string.uploading_image);
                mProgressDialog.setMessage(getResources().getString(R.string.loading_profile_mess));
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();


                Uri resultUri = result.getUri();

                File thumb_filePath = new File(Objects.requireNonNull(resultUri.getPath()));

                FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

                assert current_user != null;
                final String uid = current_user.getUid();


                Bitmap thumb_bitmap = null;
                try {
                    thumb_bitmap = new Compressor(this)
                            .setMaxWidth(200)
                            .setMaxHeight(200)
                            .setQuality(75)
                            .compressToBitmap(thumb_filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                assert thumb_bitmap != null;
                thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                final byte[] thumb_byte = baos.toByteArray();


                StorageReference filepath = mImageStorageRef.child("profile_images").child(uid + ".jpg");
                final StorageReference thumb_filepath = mImageStorageRef.child("profile_images").child("thumbs").child(uid + ".jpg");


                filepath.putFile(resultUri).addOnSuccessListener(taskSnapshot -> filepath.getDownloadUrl().addOnSuccessListener(uri -> thumb_filepath.putBytes(thumb_byte).addOnSuccessListener(taskSnapshot1 -> thumb_filepath.getDownloadUrl().addOnSuccessListener(uri_thumb -> {

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("image", String.valueOf(uri));
                    hashMap.put("thumb_image", String.valueOf(uri_thumb));


                    mUserDatabase.updateChildren(hashMap).addOnSuccessListener(aVoid -> mProgressDialog.dismiss());

                }))));
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Log.i("Error", error.toString());


            }
        }
    }
}
