package cz.derdak.writome.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.derdak.writome.MainActivity;
import cz.derdak.writome.R;
//using ButterKnife
public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.reg_display_name) TextInputLayout mDisplayName;
    @BindView(R.id.reg_display_email) TextInputLayout mEmail;
    @BindView(R.id.reg_display_pass) TextInputLayout mPass;
    @BindView(R.id.reg_sex_radiogroup) RadioGroup rgSex;
    @BindView(R.id.reg_radio_other) RadioButton rbOther;

    private DatabaseReference mDatabase;

    private ProgressDialog mRegProgress;

    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();

        Toolbar mToolbar = findViewById(R.id.register_toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.create_acc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRegProgress = new ProgressDialog(this);

        Button mCreateBtn = findViewById(R.id.btn_submit);

        rbOther.setChecked(true);

        mCreateBtn.setOnClickListener(view -> {

            String display_name = Objects.requireNonNull(mDisplayName.getEditText()).getText().toString();
            String email = Objects.requireNonNull(mEmail.getEditText()).getText().toString();
            String password = Objects.requireNonNull(mPass.getEditText()).getText().toString();

            RadioButton rb = rgSex.findViewById(rgSex.getCheckedRadioButtonId());
            String sex;
            if (rb.getText().toString() == null) {
                sex = "Default";
            } else {
                sex = rb.getText().toString();
            }

            if (TextUtils.isEmpty(display_name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {

                Toast.makeText(RegisterActivity.this, R.string.fill_all_fields, Toast.LENGTH_SHORT).show();

            } else if (!TextUtils.isEmpty(display_name) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {

                mRegProgress.setTitle(R.string.reg_user);
                mRegProgress.setMessage(getResources().getString(R.string.reg_user_mess));
                mRegProgress.setCanceledOnTouchOutside(false);
                mRegProgress.show();


                register_user(display_name, email, password, sex);
            }
        });
    }


    private void register_user(final String display_name, String email, final String password, String sex) {


        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {

            if (task.isSuccessful()) {

                FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                assert current_user != null;
                String uid = current_user.getUid();
                String deviceToken = FirebaseInstanceId.getInstance().getToken();

                mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                HashMap<String, String> userMap = new HashMap<>();
                userMap.put("device_token", deviceToken);
                userMap.put("name", display_name);
                userMap.put("status", getResources().getString(R.string.about));

                switch (sex) {

                    case "Male":
                        userMap.put("image", "default_male");
                        userMap.put("thumb_image", "default_male");
                        break;
                    case "Female":
                        userMap.put("image", "default_female");
                        userMap.put("thumb_image", "default_female");
                        break;
                    default:
                        userMap.put("image", "default_other");
                        userMap.put("thumb_image", "default_other");
                }

                userMap.put("theme", "default");

                mDatabase.setValue(userMap).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {

                        mRegProgress.dismiss();

                        Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mainIntent);
                        finish();

                    }
                });

            } else {

                mRegProgress.hide();
                Toast.makeText(RegisterActivity.this, R.string.reg_user_err, Toast.LENGTH_LONG).show();
            }

        });

    }

}

