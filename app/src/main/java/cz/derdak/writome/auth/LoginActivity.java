package cz.derdak.writome.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.derdak.writome.MainActivity;
import cz.derdak.writome.R;
//using ButterKnife
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_email) TextInputLayout mLoginEmail;
    @BindView(R.id.login_password) TextInputLayout mLoginPass;

    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase;
    private ProgressDialog mLogProgress;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        Toolbar mToolbar = findViewById(R.id.login_toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        mLogProgress = new ProgressDialog(this);

        Button mLogin_btn = findViewById(R.id.login_btn);

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        mLogin_btn.setOnClickListener(view -> {

            String email = Objects.requireNonNull(mLoginEmail.getEditText()).getText().toString();
            String pass = Objects.requireNonNull(mLoginPass.getEditText()).getText().toString();

            if (TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)) {
                Toast.makeText(LoginActivity.this, R.string.fill_all_fields, Toast.LENGTH_SHORT).show();
            } else if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(pass)) {

                mLogProgress.setTitle(R.string.logging_in);
                mLogProgress.setMessage(getResources().getString(R.string.logging_in_mess));
                mLogProgress.setCanceledOnTouchOutside(false);
                mLogProgress.show();

                loginUser(email, pass);

            }

        });

    }

    /**
     * @param email
     * @param password
     */
    private void loginUser(String email, String password) {

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {

                mLogProgress.dismiss();

                String current_user_id = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
                String deviceToken = FirebaseInstanceId.getInstance().getToken();

                if (mUserDatabase.child(current_user_id).child("name").equals(null)) {

                    Toast.makeText(getBaseContext(), "Error, user was propably deleted!", Toast.LENGTH_SHORT).show();

                } else {

                    mUserDatabase.child(current_user_id).child("device_token").setValue(deviceToken).addOnSuccessListener(aVoid -> {

                        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mainIntent);
                        finish();

                    });

                }

            } else {

                mLogProgress.hide();
                Toast.makeText(LoginActivity.this, R.string.logging_in_err, Toast.LENGTH_LONG).show();


            }

        });

    }
}
