package cz.derdak.writome;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

//

public class ChatActivity extends AppCompatActivity {

    private static final int GALLERY_PICK = 1;

    private final List<Messages> messagesList = new ArrayList<>();

    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.chat_textView) TextView chatTextView;
    @BindView(R.id.chat_message_editText) EditText chatMessageEditText;
    @BindView(R.id.chat_add_btn) ImageButton chatAddBtn;

    private String mChatUser;
    private String mCurrentUserId;

    private TextView mDescriptionView;
    private CircleImageView mProfileImage;

    private DatabaseReference mRootRef;
    private DatabaseReference mFriendDatabase;

    private MessageAdapter messageAdapter;
    private StorageReference mImageStorage;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mImageStorage = FirebaseStorage.getInstance().getReference();

        Toolbar mChatToolbar = findViewById(R.id.chat_app_bar);

        ButterKnife.bind(this);

        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        mFriendDatabase = FirebaseDatabase.getInstance().getReference().child("Friends");

        mChatUser = getIntent().getStringExtra("user_id");
        String userName = getIntent().getStringExtra("user_name");

        getSupportActionBar().setTitle(userName);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View action_bar_view = inflater.inflate(R.layout.chat_custom_bar, null);

        actionBar.setCustomView(action_bar_view);

        // Custom Action Bar items

        TextView mTitleView = findViewById(R.id.custom_bar_title);
        mDescriptionView = findViewById(R.id.custom_bar_desc);
        mProfileImage = findViewById(R.id.custom_bar_img);

        ImageButton chatSendBtn = findViewById(R.id.chat_send_btn);

        messageAdapter = new MessageAdapter(messagesList);

        RecyclerView messagesListRecyclerView = findViewById(R.id.messagesList);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);

        messagesListRecyclerView.setHasFixedSize(true);
        messagesListRecyclerView.setLayoutManager(linearLayoutManager);

        messagesListRecyclerView.setAdapter(messageAdapter);

        loadMessages();

        mTitleView.setText(userName);

        final String user_id = getIntent().getStringExtra("user_id");

        mFriendDatabase.child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {

                assert user_id != null;
                if (!dataSnapshot.hasChild(user_id)) {

                    chatSendBtn.setVisibility(View.INVISIBLE);
                    chatAddBtn.setVisibility(View.INVISIBLE);
                    chatMessageEditText.setVisibility(View.INVISIBLE);
                    chatTextView.setVisibility(View.VISIBLE);

                } else {
                    chatTextView.setVisibility(View.GONE);
                    chatSendBtn.setVisibility(View.VISIBLE);
                    chatAddBtn.setVisibility(View.VISIBLE);
                    chatMessageEditText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });

        mRootRef.child("Users").child(mChatUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {

                String online = Objects.requireNonNull(dataSnapshot.child("online").getValue()).toString();

                String image = Objects.requireNonNull(dataSnapshot.child("thumb_image").getValue()).toString();

                if (image.equals("default_male")) {
                    //    Picasso.with(ChatActivity.this).load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(mProfileImage);
                    Picasso.get().load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(mProfileImage);
                } else if (image.equals("default_female")) {
                    //    Picasso.with(ChatActivity.this).load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(mProfileImage);
                    Picasso.get().load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(mProfileImage);
                } else {
                    //   Picasso.with(ChatActivity.this).load(image).placeholder(R.drawable.avatar_other).into(mProfileImage);
                    Picasso.get().load(image).placeholder(R.drawable.avatar_other).into(mProfileImage);
                }

                if (online.equals("true")) {

                    mDescriptionView.setText(R.string.online);

                } else {

                    new TimeAgo();

                    long lastTimeSeen = Long.parseLong(online);

                    String lastSeen = TimeAgo.getTimeAgo(lastTimeSeen, getApplicationContext());

                    mDescriptionView.setText(lastSeen);
                }

            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });

        mRootRef.child("Chat").child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {

                if (!dataSnapshot.hasChild(mChatUser)) {

                    Map<String, Object> chatAddMap = new HashMap<>();
                    chatAddMap.put("seen", false);
                    chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map<String, Object> chatUserMap = new HashMap<>();
                    chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                    chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                    mRootRef.updateChildren(chatUserMap, (databaseError, databaseReference) -> {

                        if (databaseError != null) {

                            Log.d("CHAT LOG", databaseError.getMessage());

                        }

                    });

                }

            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });

        chatSendBtn.setOnClickListener(v -> sendMessage());

        chatAddBtn.setOnClickListener(v -> {
            Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, GALLERY_PICK);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {

            assert data != null;
            Uri imageUri = data.getData();

            CropImage.activity(imageUri)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                mProgressDialog = new ProgressDialog(ChatActivity.this);
                mProgressDialog.setTitle(R.string.uploading_image);
                mProgressDialog.setMessage(getResources().getString(R.string.uploading_image_mess));
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();

                assert result != null;
                Uri resultUri = result.getUri();

                File image_filePath = new File(Objects.requireNonNull(resultUri.getPath()));

                Bitmap image_bitmap = null;
                try {
                    image_bitmap = new Compressor(this)
                            .setQuality(75)
                            .compressToBitmap(image_filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                assert image_bitmap != null;
                image_bitmap.compress(Bitmap.CompressFormat.JPEG, 25, baos);
                final byte[] image_byte = baos.toByteArray();

                final String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
                final String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

                DatabaseReference user_message_push = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser).push();

                final String push_id = user_message_push.getKey();

                StorageReference filepath = mImageStorage.child("message_images").child(push_id + ".jpg");

                filepath.putBytes(image_byte).addOnSuccessListener(taskSnapshot -> filepath.getDownloadUrl().addOnSuccessListener(uri -> {

                    HashMap<String, Object> imgMessageMap = new HashMap<>();
                    imgMessageMap.put("message", String.valueOf(uri));
                    imgMessageMap.put("seen", false);
                    imgMessageMap.put("type", "image");
                    imgMessageMap.put("time", ServerValue.TIMESTAMP);
                    imgMessageMap.put("from", mCurrentUserId);

                    Map<String, Object> messageUserMap = new HashMap<>();
                    messageUserMap.put(current_user_ref + "/" + push_id, imgMessageMap);
                    messageUserMap.put(chat_user_ref + "/" + push_id, imgMessageMap);

                    chatMessageEditText.setText("");

                    mRootRef.updateChildren(messageUserMap).addOnSuccessListener(aVoid -> mProgressDialog.dismiss());
                }));

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                assert result != null;
                Exception error = result.getError();
                Log.i("Error", error.toString());

            }
        }
    }

    /*
     * Method to load messages.
     * Method to listen to child added to the database.
     * Method to refresh messagesList with new information.
     * Method to scroll position of view to bottom.
     */

    /**
     *
     */
    private void loadMessages() {

        mRootRef.child("messages").child(mCurrentUserId).child(mChatUser).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NotNull DataSnapshot dataSnapshot, String s) {

                Messages message = dataSnapshot.getValue(Messages.class);

                messagesList.add(message);
                messageAdapter.notifyDataSetChanged();
                linearLayoutManager.scrollToPosition(linearLayoutManager.getItemCount() - 1);

            }

            @Override
            public void onChildChanged(@NotNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(@NotNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NotNull DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });

    }


    private void sendMessage() {

        String message = chatMessageEditText.getText().toString();

        if (!TextUtils.isEmpty(message)) {

            DatabaseReference newNotificationRef = mRootRef.child("notifications").child(mChatUser).push();
            String newNotificationId = newNotificationRef.getKey();

            HashMap<String, String> notificationsData = new HashMap<>();
            notificationsData.put("from", mCurrentUserId);

            notificationsData.put("type", "message");
            notificationsData.put("text", message);

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            String push_id = user_message_push.getKey();


            Map<String, Object> messageMap = new HashMap<>();
            messageMap.put("seen", false);
            if (URLUtil.isValidUrl(message) || message.startsWith("www.") || (checkTLD(message) && message.contains("."))) {
                Log.i("type", "true ");
                if (!message.startsWith("http://") && !message.startsWith("https://"))
                    message = "http://" + message;
                messageMap.put("type", "url");
            } else {
                messageMap.put("type", "text");
            }
            messageMap.put("message", message);
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", mCurrentUserId);


            Map<String, Object> messageUserMap = new HashMap<>();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);
            messageUserMap.put("notifications/" + mChatUser + "/" + newNotificationId, notificationsData);

            chatMessageEditText.setText("");

            mRootRef.updateChildren(messageUserMap, (databaseError, databaseReference) -> {

                if (databaseError != null) {

                    Log.d("CHAT_LOG", databaseError.getMessage());

                }


            });


        }
    }

    public Boolean checkTLD(String message) {

        try {

            URL url = new URL("http://data.iana.org/TLD/tlds-alpha-by-domain.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String line;
            List<String> list = new ArrayList<>();
            while ((line = in.readLine()) != null) {

                list.add(line.toLowerCase());

            }
//TODO dodelat URL
            assert list != null;
            Boolean is = false;

            for (String s : list) {
                if (message.contains(s)) {
                    is = true;
                }
            }
            System.out.println(is);

            in.close();

        } catch (Exception e) {

            System.out.println(e);
        }
        return true;
    }


}
