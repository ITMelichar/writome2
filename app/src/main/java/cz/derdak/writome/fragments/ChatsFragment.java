package cz.derdak.writome.fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import cz.derdak.writome.ChatActivity;
import cz.derdak.writome.Chats;
import cz.derdak.writome.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {

    FirebaseAuth mAuth;
    String mCurrent_user_id;
    private RecyclerView mChatsList;
    private DatabaseReference mChatsDatabase;
    private DatabaseReference mUsersDatabase;
    private DatabaseReference mMessagesDatabase;

    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View mMainView = inflater.inflate(R.layout.fragment_chats, container, false);

        mChatsList = mMainView.findViewById(R.id.chats_list);
        mAuth = FirebaseAuth.getInstance();


        mCurrent_user_id = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        mChatsDatabase = FirebaseDatabase.getInstance().getReference().child("Chat").child(mCurrent_user_id);
        mMessagesDatabase = FirebaseDatabase.getInstance().getReference().child("messages").child(mCurrent_user_id);
        mChatsDatabase.keepSynced(true);
        mMessagesDatabase.keepSynced(true);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        mChatsList.setHasFixedSize(true);
        mChatsList.setLayoutManager(linearLayoutManager);


        return mMainView;

    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerOptions<Chats> options =

                new FirebaseRecyclerOptions.Builder<Chats>()
                        .setQuery(mChatsDatabase.orderByChild("timestamp"), Chats.class)
                        .build();

        FirebaseRecyclerAdapter chatRecyclerViewAdapter = new FirebaseRecyclerAdapter<Chats, ChatsFragment.ChatsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final ChatsFragment.ChatsViewHolder holder, int position, @NonNull Chats chats) {

                final String list_user_id = getRef(position).getKey();

                assert list_user_id != null;
                Query lastMessage = mMessagesDatabase.child(list_user_id).limitToLast(1);

                lastMessage.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        String data = Objects.requireNonNull(dataSnapshot.child("message").getValue()).toString();
                        //String seen = dataSnapshot.child("seen").getValue().toString();
                        //if(seen == "true") {
                        holder.setMessage(data, chats.isSeen());
                        // } else {

                        // }
                          /*
                            userNameView.setTypeface(userNameView.getTypeface(), Typeface.BOLD_ITALIC);
                        } else {
                            userNameView.setTypeface(userNameView.getTypeface(), Typeface.NORMAL);
                        }*/

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                        final String userName = Objects.requireNonNull(dataSnapshot.child("name").getValue()).toString();
                        String userThumb = Objects.requireNonNull(dataSnapshot.child("thumb_image").getValue()).toString();

                        holder.setName(userName);
                        holder.setUserImage(userThumb, getContext());
                        // When Click to Chats View
                        holder.mView.setOnClickListener(v -> {

                            Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                            chatIntent.putExtra("user_id", list_user_id);
                            chatIntent.putExtra("user_name", userName);
                            startActivity(chatIntent);

                            // Set Seen TRUE
                            Map<String, Object> hashMap = new HashMap<>();
                            hashMap.put("seen", true);
                            mChatsDatabase.child(list_user_id).updateChildren(hashMap).addOnSuccessListener((OnSuccessListener) o -> Log.i("seen", "true"));

                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public ChatsFragment.ChatsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_user_layout, parent, false);

                return new ChatsFragment.ChatsViewHolder(view);
            }
        };

        mChatsList.setAdapter(chatRecyclerViewAdapter);
        chatRecyclerViewAdapter.startListening();
    }

    public static class ChatsViewHolder extends RecyclerView.ViewHolder {

        View mView;

        ChatsViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        void setMessage(String message, boolean isSeen) {

            TextView userNameView = mView.findViewById(R.id.user_single_status);
            userNameView.setText(message);

            if (!isSeen) {
                userNameView.setTypeface(userNameView.getTypeface(), Typeface.BOLD_ITALIC);
            } else {
                userNameView.setTypeface(userNameView.getTypeface(), Typeface.NORMAL);
            }

        }

       /* void setMessage(String message) {

            TextView userNameView = mView.findViewById(R.id.user_single_status);
            userNameView.setText(message);

        }*/

        public void setName(String name) {

            TextView userNameView = mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }

        void setUserImage(String thumb_image, Context ctx) {

            CircleImageView userImageView = mView.findViewById(R.id.user_single_image);
            if (thumb_image.equals("default_male")) {
                //    Picasso.with(ctx).load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(userImageView);
            } else if (thumb_image.equals("default_female")) {
                //    Picasso.with(ctx).load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(userImageView);
            } else {
                //    Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(thumb_image).placeholder(R.drawable.avatar_other).into(userImageView);
            }

        }


    }

}

