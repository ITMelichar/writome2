package cz.derdak.writome.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import cz.derdak.writome.ProfileActivity;
import cz.derdak.writome.R;
import cz.derdak.writome.Requests;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestFragment extends Fragment {

    private RecyclerView mRequestsList;

    private DatabaseReference mRequestsDatabase;
    private DatabaseReference mUsersDatabase;


    public RequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View mMainView = inflater.inflate(R.layout.fragment_request, container, false);

        mRequestsList = mMainView.findViewById(R.id.requests_list);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();


        String mCurrent_user_id = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        mRequestsDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req").child(mCurrent_user_id);
        mRequestsDatabase.keepSynced(true);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);

        mRequestsList.setHasFixedSize(true);
        mRequestsList.setLayoutManager(new LinearLayoutManager(getContext()));

        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerOptions<Requests> options =

                new FirebaseRecyclerOptions.Builder<Requests>()
                        .setQuery(mRequestsDatabase, Requests.class)
                        .build();

        FirebaseRecyclerAdapter requestRecyclerViewAdapter = new FirebaseRecyclerAdapter<Requests, RequestFragment.RequestsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final RequestFragment.RequestsViewHolder holder, int position, @NonNull Requests requests) {
                holder.setRequest_type(requests.getRequest_type());

                final String list_user_id = getRef(position).getKey();

                mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final String userName = Objects.requireNonNull(dataSnapshot.child("name").getValue()).toString();
                        String userThumb = Objects.requireNonNull(dataSnapshot.child("thumb_image").getValue()).toString();

                        holder.setName(userName);
                        holder.setUserImage(userThumb, getContext());
                        // When Click to Requests View
                        holder.mView.setOnClickListener(v -> {

                            Intent profileIntent = new Intent(getContext(), ProfileActivity.class);
                            profileIntent.putExtra("user_id", list_user_id);
                            startActivity(profileIntent);

                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public RequestFragment.RequestsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_user_layout, parent, false);

                return new RequestFragment.RequestsViewHolder(view);
            }
        };

        mRequestsList.setAdapter(requestRecyclerViewAdapter);
        requestRecyclerViewAdapter.startListening();
    }

    public static class RequestsViewHolder extends RecyclerView.ViewHolder {

        View mView;

        RequestsViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        void setRequest_type(String type) {

            TextView userNameView = mView.findViewById(R.id.user_single_status);
            if (type.equals("sent")) {
                userNameView.setText(R.string.friend_request_sent);
            } else {
                userNameView.setText(R.string.friend_request_received);
            }
        }

        public void setName(String name) {

            TextView userNameView = mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }

        void setUserImage(String thumb_image, Context ctx) {

            CircleImageView userImageView = mView.findViewById(R.id.user_single_image);
            if (thumb_image.equals("default_male")) {
                //  Picasso.with(ctx).load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(userImageView);
            } else if (thumb_image.equals("default_female")) {
                //  Picasso.with(ctx).load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(userImageView);
            } else {
                //  Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(thumb_image).placeholder(R.drawable.avatar_other).into(userImageView);
            }

        }


    }

}
