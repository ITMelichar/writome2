package cz.derdak.writome.fragments;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import cz.derdak.writome.R;

public class SectionsPageAdapter extends FragmentPagerAdapter {

    private Context _context;

    public SectionsPageAdapter(FragmentManager fm, Context pT) {
        super(fm);
        _context = pT;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new RequestFragment();

            case 1:
                return new ChatsFragment();

            case 2:
                return new FriendsFragment();

            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return _context.getResources().getString(R.string.req);
            case 1:
                return _context.getResources().getString(R.string.chats);
            case 2:
                return _context.getResources().getString(R.string.friends);
            default:
                return null;
        }

    }
}

