package cz.derdak.writome.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import cz.derdak.writome.ChatActivity;
import cz.derdak.writome.Friends;
import cz.derdak.writome.ProfileActivity;
import cz.derdak.writome.R;
import cz.derdak.writome.UsersActivity;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {

    String mCurrent_user_id;
    private RecyclerView mFriendsList, mUsersList;
    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;
    private MaterialButton mButton;


    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mMainView = inflater.inflate(R.layout.fragment_friends, container, false);

        mFriendsList = mMainView.findViewById(R.id.friends_list);
        // mUsersList = mMainView.findViewById(R.id.users_list);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mButton = mMainView.findViewById(R.id.usersButton);

        mCurrent_user_id = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("Friends").child(mCurrent_user_id);
        mFriendsDatabase.keepSynced(true);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);

        mFriendsList.setHasFixedSize(true);
        mFriendsList.setLayoutManager(new LinearLayoutManager(getContext()));

        /*
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(new LinearLayoutManager(getContext()));
        */

        mButton.setOnClickListener(v -> {
            Intent startIntent = new Intent(getContext(), UsersActivity.class);
            startActivity(startIntent);
        });

        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        loadFriends();

    }

    //Function to load friends list
    void loadFriends() {
        FirebaseRecyclerOptions<Friends> friendsOptions =

                new FirebaseRecyclerOptions.Builder<Friends>()
                        .setQuery(mFriendsDatabase, Friends.class)
                        .build();

        FirebaseRecyclerAdapter friendRecyclerViewAdapter = new FirebaseRecyclerAdapter<Friends, FriendsViewHolder>(friendsOptions) {
            @Override
            protected void onBindViewHolder(@NonNull final FriendsViewHolder holder, int position, @NonNull Friends friends) {
                holder.setDate(friends.getDate());

                final String list_user_id = getRef(position).getKey();

                assert list_user_id != null;
                mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                        final String userName = Objects.requireNonNull(dataSnapshot.child("name").getValue()).toString();
                        String userThumb = Objects.requireNonNull(dataSnapshot.child("thumb_image").getValue()).toString();

                        if (dataSnapshot.hasChild("online")) {
                            String userOnline = Objects.requireNonNull(dataSnapshot.child("online").getValue()).toString();
                            holder.setUserOnline(userOnline);
                        }

                        holder.setName(userName);
                        holder.setUserImage(userThumb, getContext());
                        // When Click to Friends View
                        holder.mView.setOnClickListener(v -> {

                            CharSequence[] options1 = new CharSequence[]{Objects.requireNonNull(getContext()).getResources().getString(R.string.open_profile), Objects.requireNonNull(getContext()).getResources().getString(R.string.send_message)};

                            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                            builder.setTitle(R.string.select_option);
                            builder.setItems(options1, (dialog, which) -> {
                                //click event for each item
                                if (which == 0) {
                                    Intent profileIntent = new Intent(getContext(), ProfileActivity.class);
                                    profileIntent.putExtra("user_id", list_user_id);
                                    startActivity(profileIntent);
                                }
                                if (which == 1) {
                                    Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                    chatIntent.putExtra("user_id", list_user_id);
                                    chatIntent.putExtra("user_name", userName);
                                    startActivity(chatIntent);
                                }
                            });

                            builder.show();
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_user_layout, parent, false);

                return new FriendsViewHolder(view);
            }
        };

        mFriendsList.setAdapter(friendRecyclerViewAdapter);
        friendRecyclerViewAdapter.startListening();
    }

    public static class FriendsViewHolder extends RecyclerView.ViewHolder {

        View mView;

        FriendsViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        void setDate(String date) {

            TextView userNameView = mView.findViewById(R.id.user_single_status);
            userNameView.setText(date);

        }

        public void setName(String name) {

            TextView userNameView = mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }

        void setUserImage(String thumb_image, Context ctx) {

            CircleImageView userImageView = mView.findViewById(R.id.user_single_image);
            if (thumb_image.equals("default_male")) {
                Picasso.get().load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(userImageView);
            } else if (thumb_image.equals("default_female")) {
                Picasso.get().load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(userImageView);
            } else {
                Picasso.get().load(thumb_image).placeholder(R.drawable.avatar_other).into(userImageView);
            }

        }

        void setUserOnline(String online_status) {

            ImageView userOnlineView = mView.findViewById(R.id.user_single_online_icon);

            if (online_status.equals("true")) {
                userOnlineView.setVisibility(View.VISIBLE);
            } else {
                userOnlineView.setVisibility(View.INVISIBLE);
            }
        }


    }

}
