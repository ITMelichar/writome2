package cz.derdak.writome

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity


class SplashScreen : AppCompatActivity() {


    private var rotateAnim: Animation? = null
    var logoView: ImageView? = null
    private val time = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        Handler().postDelayed({
            val intent = Intent(this@SplashScreen, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, time.toLong())

        logoView = findViewById(R.id.splashImageView)

        rotateAnimation()


    }

    private fun rotateAnimation() {
        rotateAnim = AnimationUtils.loadAnimation(this@SplashScreen, R.anim.rotate)
        logoView?.startAnimation(rotateAnim)
    }
}
