package cz.derdak.writome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class UsersActivity extends AppCompatActivity {

    private RecyclerView mUsersList;
    private String mCurrent_user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        Toolbar mToolbar = findViewById(R.id.users_appBar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.all_users);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mCurrent_user = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();

        mUsersList = findViewById(R.id.users_list);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(new LinearLayoutManager(this));

        startListening();


    }

    public void startListening() {
        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("Users")
                .limitToLast(50);

        FirebaseRecyclerOptions<Users> options =
                new FirebaseRecyclerOptions.Builder<Users>()
                        .setQuery(query, Users.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<Users, UserViewHolder>(options) {
            @Override
            public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_user_layout, parent, false);

                return new UserViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(UserViewHolder holder, int position, Users model) {

                holder.setName(model.name);
                holder.setStatus(model.status);
                holder.setUserImage(model.getThumb_image(), getApplicationContext());

                final String user_id = getRef(position).getKey();
                final String user_status = getRef(position).toString();
                final String user_name = FirebaseDatabase.getInstance().getReference().child("Users").child("name").getKey();

                holder.mView.setOnClickListener(view -> {

                    if (user_id == mCurrent_user) {

                        Intent myProfileIntent = new Intent(UsersActivity.this, SettingsActivity.class);
                        startActivity(myProfileIntent);

                    } else {

                        Intent profileIntent = new Intent(UsersActivity.this, ProfileActivity.class);
                        profileIntent.putExtra("user_id", user_id);
                        profileIntent.putExtra("user_name", user_name);
                        profileIntent.putExtra("user_status", user_status);
                        startActivity(profileIntent);
                    }
                });
            }

        };
        mUsersList.setAdapter(adapter);
        adapter.startListening();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        View mView;

        UserViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setName(String name) {
            TextView userNameView = mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);
        }

        void setStatus(String status) {
            TextView statusView = mView.findViewById(R.id.user_single_status);
            statusView.setText(status);
        }

        void setUserImage(String thumb_image, Context ctx) {

            CircleImageView userImageView = mView.findViewById(R.id.user_single_image);


            if (thumb_image.equals("default_male")) {
                //    Picasso.with(ctx).load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(R.drawable.avatar_male).placeholder(R.drawable.avatar_other).into(userImageView);
            } else if (thumb_image.equals("default_female")) {
                //    Picasso.with(ctx).load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(R.drawable.avatar_female).placeholder(R.drawable.avatar_other).into(userImageView);
            } else {
                //    Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.avatar_other).into(userImageView);
                Picasso.get().load(thumb_image).placeholder(R.drawable.avatar_other).into(userImageView);
            }
        }


    }
}
