package cz.derdak.writome;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

@SuppressLint("Registered")
public class TimeAgo extends Application {

    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 60 * HOUR;

    public static String getTimeAgo(long time, Context context) {
        if (time < 1000000000000L) {
            time += 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        final long difference = now - time;
        if (difference < MINUTE) {
            return context.getResources().getString(R.string.just_now);
        } else if (difference < 2 * MINUTE) {
            return context.getResources().getString(R.string.minute_ago);
        } else if (difference < 50 * MINUTE) {
            return difference / MINUTE + " " + context.getResources().getString(R.string.minutes_ago);
        } else if (difference < 90 * MINUTE) {
            return context.getResources().getString(R.string.hour_ago);
        } else if (difference < 24 * HOUR) {
            return difference / HOUR + " " + context.getResources().getString(R.string.hours_ago);
        } else if (difference < 48 * HOUR) {
            return context.getResources().getString(R.string.yesterday);
        } else {
            return difference / DAY + " " + context.getResources().getString(R.string.days_ago);
        }

    }


}
