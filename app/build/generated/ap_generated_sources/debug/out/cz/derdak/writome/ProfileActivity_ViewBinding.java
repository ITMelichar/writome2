// Generated code from Butter Knife. Do not modify!
package cz.derdak.writome;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileActivity_ViewBinding implements Unbinder {
  private ProfileActivity target;

  @UiThread
  public ProfileActivity_ViewBinding(ProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfileActivity_ViewBinding(ProfileActivity target, View source) {
    this.target = target;

    target.mProfileImage = Utils.findRequiredViewAsType(source, R.id.profile_image, "field 'mProfileImage'", ImageView.class);
    target.mDisplayName = Utils.findRequiredViewAsType(source, R.id.displayName, "field 'mDisplayName'", TextView.class);
    target.mDisplayStatus = Utils.findRequiredViewAsType(source, R.id.displayStatus, "field 'mDisplayStatus'", TextView.class);
    target.mProfileSendReq = Utils.findRequiredViewAsType(source, R.id.profile_send_req_btn, "field 'mProfileSendReq'", Button.class);
    target.mProfileDeclineReq = Utils.findRequiredViewAsType(source, R.id.profile_decline_req_btn, "field 'mProfileDeclineReq'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mProfileImage = null;
    target.mDisplayName = null;
    target.mDisplayStatus = null;
    target.mProfileSendReq = null;
    target.mProfileDeclineReq = null;
  }
}
