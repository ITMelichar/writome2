// Generated code from Butter Knife. Do not modify!
package cz.derdak.writome.auth;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.textfield.TextInputLayout;
import cz.derdak.writome.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterActivity_ViewBinding implements Unbinder {
  private RegisterActivity target;

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target, View source) {
    this.target = target;

    target.mDisplayName = Utils.findRequiredViewAsType(source, R.id.reg_display_name, "field 'mDisplayName'", TextInputLayout.class);
    target.mEmail = Utils.findRequiredViewAsType(source, R.id.reg_display_email, "field 'mEmail'", TextInputLayout.class);
    target.mPass = Utils.findRequiredViewAsType(source, R.id.reg_display_pass, "field 'mPass'", TextInputLayout.class);
    target.rgSex = Utils.findRequiredViewAsType(source, R.id.reg_sex_radiogroup, "field 'rgSex'", RadioGroup.class);
    target.rbOther = Utils.findRequiredViewAsType(source, R.id.reg_radio_other, "field 'rbOther'", RadioButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDisplayName = null;
    target.mEmail = null;
    target.mPass = null;
    target.rgSex = null;
    target.rbOther = null;
  }
}
