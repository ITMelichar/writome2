// Generated code from Butter Knife. Do not modify!
package cz.derdak.writome;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChatActivity_ViewBinding implements Unbinder {
  private ChatActivity target;

  @UiThread
  public ChatActivity_ViewBinding(ChatActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChatActivity_ViewBinding(ChatActivity target, View source) {
    this.target = target;

    target.chatTextView = Utils.findRequiredViewAsType(source, R.id.chat_textView, "field 'chatTextView'", TextView.class);
    target.chatMessageEditText = Utils.findRequiredViewAsType(source, R.id.chat_message_editText, "field 'chatMessageEditText'", EditText.class);
    target.chatAddBtn = Utils.findRequiredViewAsType(source, R.id.chat_add_btn, "field 'chatAddBtn'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChatActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.chatTextView = null;
    target.chatMessageEditText = null;
    target.chatAddBtn = null;
  }
}
