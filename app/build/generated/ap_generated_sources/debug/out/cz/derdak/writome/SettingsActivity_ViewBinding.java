// Generated code from Butter Knife. Do not modify!
package cz.derdak.writome;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingsActivity_ViewBinding implements Unbinder {
  private SettingsActivity target;

  @UiThread
  public SettingsActivity_ViewBinding(SettingsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SettingsActivity_ViewBinding(SettingsActivity target, View source) {
    this.target = target;

    target.mDisplayImage = Utils.findRequiredViewAsType(source, R.id.circleImage, "field 'mDisplayImage'", CircleImageView.class);
    target.mName = Utils.findRequiredViewAsType(source, R.id.displayID, "field 'mName'", TextView.class);
    target.mStatus = Utils.findRequiredViewAsType(source, R.id.sett_text_about, "field 'mStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SettingsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDisplayImage = null;
    target.mName = null;
    target.mStatus = null;
  }
}
