// Generated code from Butter Knife. Do not modify!
package cz.derdak.writome;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StartActivity_ViewBinding implements Unbinder {
  private StartActivity target;

  @UiThread
  public StartActivity_ViewBinding(StartActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public StartActivity_ViewBinding(StartActivity target, View source) {
    this.target = target;

    target.mRegBtn = Utils.findRequiredViewAsType(source, R.id.btn_registration, "field 'mRegBtn'", Button.class);
    target.mLogBtn = Utils.findRequiredViewAsType(source, R.id.btn_login, "field 'mLogBtn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StartActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRegBtn = null;
    target.mLogBtn = null;
  }
}
