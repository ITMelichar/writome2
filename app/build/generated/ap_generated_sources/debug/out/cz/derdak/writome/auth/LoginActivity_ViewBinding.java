// Generated code from Butter Knife. Do not modify!
package cz.derdak.writome.auth;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.textfield.TextInputLayout;
import cz.derdak.writome.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target, View source) {
    this.target = target;

    target.mLoginEmail = Utils.findRequiredViewAsType(source, R.id.login_email, "field 'mLoginEmail'", TextInputLayout.class);
    target.mLoginPass = Utils.findRequiredViewAsType(source, R.id.login_password, "field 'mLoginPass'", TextInputLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mLoginEmail = null;
    target.mLoginPass = null;
  }
}
